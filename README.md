[![pipeline status](https://git.coop/aptivate/ansible-roles/epel-repository/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/epel-repository/commits/master)

# epel-repository

A role to configure the EPEL repository.

# Requirements

None.

# Role Variables

None.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: epel-repository
```

# Testing


```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
